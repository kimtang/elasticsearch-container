# Pull base image.
FROM docker.elastic.co/elasticsearch/elasticsearch:6.4.1
ENV discovery.type single-node

# Define mountable directories.
VOLUME ["/data"]

# Define working directory.
WORKDIR /data

# Define default command.
CMD ["/elasticsearch/bin/elasticsearch"]

# Mount jwtrealm plugin directory
ADD jwtrealm.zip /usr/share/elasticsearch/PluginsToInstall/jwtrealm.zip

# Mount elasticsearch.yml config
ADD elasticsearch.yml /usr/share/elasticsearch/config/elasticsearch.yml

# Mount elasticsearch certificate and move it to cert folder (Following https://www.elastic.co/guide/en/elastic-stack-overview/6.4/ssl-tls.html)
ADD elastic-cert.p12 /usr/share/elasticsearch/config/certs/elastic-cert.p12

# Install icu plugin
RUN /usr/share/elasticsearch/bin/elasticsearch-plugin install analysis-icu

# Install jwt-realm plugin (custom installation)
RUN /usr/share/elasticsearch/bin/elasticsearch-plugin install file:///usr/share/elasticsearch/PluginsToInstall/jwtrealm.zip

# List installed plugins 
RUN elasticsearch-plugin list

# Expose ports.
#- 9200: HTTP
EXPOSE 9200 
#- 9300: transport
EXPOSE 9300 