Project to dockerize elasticsearch for https://zontal.atlassian.net/browse/ZSPACE-6555

- elasticsearch version 6.4.1 
- elasticsearch.yml is being mounted
- icu-analysis plugin is being installed
- jwt-plugin is being cloned from space-services and then installed
- for tls, a certificate is being mounted and xpack configured for it in in elasticsearch.yml